package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"time"
)

//
var col_size int = 10
var row_size int = 20

//
var shape_o = [][]int{
	{1, 1},
	{1, 1}}
var shape_l = [][]int{
	{1, 0, 0},
	{1, 0, 0},
	{1, 1, 0}}
var shape_j = [][]int{
	{0, 1, 0},
	{0, 1, 0},
	{1, 1, 0}}
var shape_s = [][]int{
	{1, 0, 0},
	{1, 1, 0},
	{0, 1, 0}}
var shape_z = [][]int{
	{1, 1, 0},
	{0, 1, 1},
	{0, 0, 0}}
var shape_i = [][]int{
	{0, 0, 0, 0},
	{1, 1, 1, 1},
	{0, 0, 0, 0},
	{0, 0, 0, 0}}
var shape_t = [][]int{
	{0, 1, 0},
	{1, 1, 1},
	{0, 0, 0}}

//
var color = []string{
	"\033[0;31;41m", //Text: Red, Background: Red
	"\033[0;32;42m", //Text: Green, Background: Green
	"\033[0;33;43m", //Text: Yellow, Background: Yellow
	"\033[0;34;44m", //Text: Blue, Background: Blue
	"\033[0;35;45m", //Text: Purple, Background: Purple
	"\033[0;36;46m", //Text: Cyan, Background: Cyan
	"\033[0;37;47m"} //Text: White, Background: White
//
var shape_of_block = [][][]int{shape_i, shape_j, shape_l, shape_o, shape_s, shape_t, shape_z}

//
type Block struct {
	x              int
	y              int
	shape          [][]int
	original_shape [][]int
}

//
func (block *Block) reinit_block() {
	// rand.Seed(time.Now().UnixNano()) //in case receiving the same data at a lot of next time

	rand_num := rand.Intn(7-1) + 1
	for i := range block.shape {
		for j := range block.shape[0] {
			if block.shape[i][j] != 0 {
				//assign to random number we got previously
				block.shape[i][j] = rand_num
				block.original_shape[i][j] = rand_num
			}
		}
	}
}

//
func (block *Block) transpose() {
	for i := range block.original_shape {
		for j := 0 + i; j < len(block.original_shape[0]); j++ {
			//swap block.original_shape index [j][i] with [i][j]
			block.original_shape[j][i], block.original_shape[i][j] = block.original_shape[i][j], block.original_shape[j][i]
		}
	}
}

//
func (block *Block) reverse_column() {
	temp := 0
	pivot := int(len(block.original_shape[0]) / 2)
	for i := range block.original_shape {
		for j := 0; j < pivot; j++ {
			temp = block.original_shape[i][j]
			block.original_shape[i][j] = block.original_shape[i][len(block.original_shape[0])-1-j]
			block.original_shape[i][len(block.original_shape[0])-1-j] = temp
		}
	}
}

//
func (block *Block) rotate() {
	block.transpose()
	block.reverse_column()
	block.shape = make_copy(block.original_shape)
	block.cut_shape()
}

// check left and right side whether block is surround by another block (pieces) or not
func check_left_right(land [][]int, tempBlock1 Block) bool {
	var flag1, flag2 bool
	var left, right, pivot int
	temp_x := tempBlock1.x
	temp_y := tempBlock1.y
	pivot = int(len(tempBlock1.shape[0]) / 2)
	for i := range tempBlock1.shape {
		left = 0
		right = len(tempBlock1.shape[0]) - 1
		if tempBlock1.y+i >= 0 && tempBlock1.y+i <= row_size-1 && tempBlock1.x >= 0 && tempBlock1.x+len(tempBlock1.shape[0])-1 <= col_size-1 {
			for left < pivot {
				if land[tempBlock1.y+i][tempBlock1.x+left] != 0 {
					flag1 = true
					break
				} else {
					left++
				}
			}
			for right > pivot+1 {
				if land[tempBlock1.y+i][tempBlock1.x+right] != 0 {
					flag2 = true
					break
				} else {
					right--
				}
			}
		} else if tempBlock1.x < 0 {
			for tempBlock1.x < 0 {
				tempBlock1.x++
			}
			for right > pivot+1 {
				if land[tempBlock1.y+i][tempBlock1.x+right] != 0 {
					tempBlock1.x = temp_x
					tempBlock1.y = temp_y
					return true
				} else {
					right--
				}
			}
		} else if tempBlock1.x+len(tempBlock1.shape[0])-1 > col_size-1 {
			for tempBlock1.x+len(tempBlock1.shape[0])-1 > col_size-1 {
				tempBlock1.x--
			}
			for left < pivot {
				if land[tempBlock1.y+i][tempBlock1.x+left] != 0 {
					tempBlock1.x = temp_x
					tempBlock1.y = temp_y
					return true
				} else {
					left++
				}
			}
		}
	}
	return (flag1 && flag2)
}

//compare 2 2D-array is the same or not
//require two same size array
func Compare2DArray(arr1 [][]int, arr2 [][]int) bool {
	for i := range arr1 {
		for j := range arr1[0] {
			if arr1[i][j] != arr2[i][j] {
				return false
			}
		}
	}
	return true
}

//most difficult function in my opinion
func (block *Block) rotate_modify(land [][]int) {
	//rotated temporary block
	tempBlock1 := Block{block.x, block.y, make_copy(block.shape), make_copy(block.original_shape)}
	tempBlock1.rotate()
	//unrotated temporary block
	tempBlock2 := Block{block.x, block.y, make_copy(block.shape), make_copy(block.original_shape)}
	flag := false
	maxUp := 2
	//if block is surrounded => can't rotate
	if check_collision(land, tempBlock1) {
		if check_left_right(land, tempBlock1) {
			return
		}
		//
		for tempBlock1.x < 0 && check_collision(land, tempBlock1) {
			tempBlock2.x++
			tempBlock1.x++
			if !check_collision(land, tempBlock1) {
				flag = true
				break
			} else if check_collision(land, tempBlock2) {
				tempBlock2.x--
				tempBlock1.x--
				break
			}
		}
		for tempBlock1.x+len(tempBlock1.shape[0])-1 > col_size-1 && check_collision(land, tempBlock1) {
			if flag == true {
				break
			}
			tempBlock2.x--
			tempBlock1.x--
			if !check_collision(land, tempBlock1) {
				flag = true
				break
			} else if check_collision(land, tempBlock2) {
				tempBlock2.x--
				tempBlock1.x--
				break
			}
		}
		for tempBlock1.y+len(tempBlock1.shape)-1 > row_size-1 && check_collision(land, tempBlock1) {
			if flag == true {
				break
			}
			tempBlock2.y--
			tempBlock1.y--
			if !check_collision(land, tempBlock1) {
				flag = true
				break
			} else if check_collision(land, tempBlock2) {
				tempBlock2.y++
				tempBlock1.y++
				break
			}
		}
		// check bottom side of block
		k := 0
		for j := range tempBlock1.shape[0] {
			if flag == true {
				break
			}
			k = 0
			//check bottom
			for tempBlock1.shape[len(tempBlock1.shape)-1-k][j] == 0 {
				k++
			}
			if tempBlock1.y+len(tempBlock1.shape)-1-k >= 0 && tempBlock1.y+len(tempBlock1.shape)-1-k <= row_size-1 && tempBlock1.x+j >= 0 && tempBlock1.x+j <= col_size-1 {
				for land[tempBlock1.y+len(tempBlock1.shape)-1-k][tempBlock1.x+j] != 0 && maxUp != 0 {
					tempBlock2.y--
					tempBlock1.y--
					maxUp--
					if !check_collision(land, tempBlock1) {
						flag = true
						break
					} else if check_collision(land, tempBlock2) {
						tempBlock2.y++
						tempBlock1.y++
						break
					}
				}
			}
		}
		f := 0
		//check right-left side of block
		for i := range tempBlock1.shape {
			if flag == true {
				break
			}
			f = 0
			//left side
			for tempBlock1.shape[i][0+f] == 0 && f <= len(tempBlock1.shape[0])-1 {
				f++
			}
			if tempBlock1.shape[i][0+f] != 0 && tempBlock1.y+i >= 0 && tempBlock1.y+i <= row_size-1 && tempBlock1.x+f >= 0 && tempBlock1.x+f <= col_size-1 {
				for land[tempBlock1.y+i][tempBlock1.x+f] != 0 && check_collision(land, tempBlock1) {
					tempBlock1.x++
					tempBlock2.x++
					if !check_collision(land, tempBlock1) {
						flag = true
						break
					} else if check_collision(land, tempBlock2) {
						tempBlock2.x--
						tempBlock1.x--
						break
					}
				}
			}
			f = 0
			//right side
			for tempBlock1.shape[i][len(tempBlock1.shape[0])-1-f] == 0 && f <= len(tempBlock1.shape[0])-1 {
				f++
			}
			if tempBlock1.shape[i][len(tempBlock1.shape[0])-1-f] != 0 && tempBlock1.y+i >= 0 && tempBlock1.y+i <= row_size-1 && tempBlock1.x+len(tempBlock1.shape[0])-1-f >= 0 && tempBlock1.x+len(tempBlock1.shape[0])-1-f <= col_size-1 {
				for land[tempBlock1.y+i][tempBlock1.x+len(tempBlock1.shape[0])-1-f] != 0 && check_collision(land, tempBlock1) {
					tempBlock1.x--
					tempBlock2.x--
					if !check_collision(land, tempBlock1) {
						flag = true
						break
					} else if check_collision(land, tempBlock2) {
						tempBlock2.x++
						tempBlock1.x++
						break
					}
				}
			}
		}
	} else {
		block.shape = make_copy(tempBlock1.shape)
		block.original_shape = make_copy(tempBlock1.original_shape)
	}
	if flag == true {
		block.x = tempBlock1.x
		block.y = tempBlock1.y
		block.shape = make_copy(tempBlock1.shape)
		block.original_shape = make_copy(tempBlock1.original_shape)
		return
	}
}

//relocating after rotating
// func (block *Block) reLocate() {
// 	flag := false
// 	//relocate coordinate y
// 	for i := range block.original_shape {
// 		for j := range block.original_shape[0] {
// 			if block.original_shape[i][j] != 0 {
// 				block.y += i
// 				flag = true
// 			}
// 		}
// 		if flag == true {break}
// 	}
// 	flag = false
// 	//relocate coordintate x
// 	for i := range block.original_shape[0] {
// 		for j := range block.original_shape {
// 			if block.original_shape[j][i] != 0 {
// 				block.x += i
// 				flag = true
// 			}
// 		}
// 		if flag == true {break}
// 	}
// }
//
func (block *Block) cut_shape() {
	var col []int
	var row []int
	var flag bool
	//check columns
	for i := range block.shape[0] {
		flag = true
		for j := range block.shape {
			if block.shape[j][i] != 0 {
				flag = false
			}
		}
		if flag == true {
			col = append(col, i)
		}
	}
	//check rows
	for i := range block.shape {
		flag = true
		for j := range block.shape[0] {
			if block.shape[i][j] != 0 {
				flag = false
			}
		}
		if flag == true {
			row = append(row, i)
		}
	}
	pos := 0 //temporary position variable
	//delete unnecessary rows
	for i := 0; i < len(row); i++ {
		pos = row[len(row)-1-i]
		block.shape = append(block.shape[:pos], block.shape[pos+1:]...)
	}
	//delete unnecessary columns
	for i := 0; i < len(col); i++ {
		pos = col[len(col)-1-i]
		for j := 0; j < len(block.shape); j++ {
			block.shape[j] = append(block.shape[j][:pos], block.shape[j][pos+1:]...)
		}
	}
}

//
func (block *Block) move_down(land [][]int) {
	if check_bottom(land, *block) {
		return
	}
	block.y++
	if check_collision(land, *block) {
		block.y--
	}
}

//
func (block *Block) move_left(land [][]int) {
	if block.x > 0 {
		block.x--
	}
	if check_collision(land, *block) {
		block.x++
	}
}

//
func (block *Block) move_right(land [][]int) {
	if block.x+len(block.shape[0])-1 < col_size-1 {
		block.x++
	}
	if check_collision(land, *block) {
		block.x--
	}
}

//
func (block *Block) hard_drop(land [][]int) {
	for !check_bottom(land, *block) {
		block.y++
	}
}

//
func merge_block(board [][]int, block Block) [][]int {
	for i := range block.shape {
		for j := range block.shape[0] {
			if block.shape[i][j] != 0 {
				if block.y+i <= row_size-1 && block.y+i >= 0 && block.x+j >= 0 && block.x+j <= col_size-1 {
					board[block.y+i][block.x+j] = block.shape[i][j]
				}
			}
		}
	}
	return board
}

//
func check_collision(land [][]int, block Block) bool {
	if block.x < 0 || block.x+len(block.shape[0])-1 > col_size-1 || block.y+len(block.shape)-1 > row_size-1 {
		return true
	}
	for i := range block.shape {
		for j := range block.shape[0] {
			//Condition for any block that hasn't drop in board (means outside of board)
			if block.y+i >= 0 {
				if land[block.y+i][block.x+j] != 0 && block.shape[i][j] != 0 {
					return true
				}
			}
		}
	}
	return false
}

//
func check_bottom(land [][]int, block Block) bool {
	if block.y+len(block.shape)-1 >= row_size-1 {
		return true
	}
	//check from the bottom to the top of block until meet piece has value == 1
	var i int = 0
	for j := 0; j < len(block.shape[0]); j++ {
		if block.shape[len(block.shape)-1-i][j] != 0 {
			if block.y+len(block.shape)-i >= 0 && block.y+len(block.shape)-i <= row_size-1 && block.x+j >= 0 && block.x+j <= col_size-1 {
				if land[block.y+len(block.shape)-i][block.x+j] != 0 {
					i = 0
					return true
				} else {
					i = 0
				}
			} else {
				i = 0
			}
		} else {
			//check column j again due to block.shape[i][j] == 0 (we want to check under block's piece, which has value 1)
			i++
			j--
		}
	}
	return false
}

//
func reach_top(land [][]int) bool {
	for _, item := range land[0] {
		if item != 0 {
			return true
		}
	}
	return false
}

//
func full_line(land [][]int, score int) ([][]int, int) {
	var flag bool
	var line []int
	//push above rows down (swap) len(line)-2 times
	for i := range land {
		flag = true
		for j := range land[0] {
			if land[i][j] == 0 {
				flag = false
			}
		}
		if flag == true {
			line = append(line, i)
		}
	}
	for _, i := range line {
		for j := range land[0] {
			land[i][j] = 0
		}
		for j := i; j > 0; j-- {
			land[j-1], land[j] = land[j], land[j-1]
		}
		score += 100 //get scores
	}
	return land, score
}

//
func init_board() [][]int {
	board := make([][]int, row_size) //auto initialize as 0
	for i := range board {
		board[i] = make([]int, col_size)
	}
	return board
}

//
func rand_block(shape [][][]int, board [][]int) Block {
	// rand.Seed(time.Now().UnixNano()) //in case receiving the same data at a lot of next time

	rand_shape := make_copy(shape[rand.Intn(len(shape)-1)])
	// rand_shape := make_copy(shape_i)
	block := Block{int(len(board[0])/2) - len(rand_shape[0]) + 1, -2, rand_shape, make_copy(rand_shape)}
	block.reinit_block()
	block.cut_shape()
	return block
}

//
func make_copy(arr [][]int) [][]int {
	temp_arr := make([][]int, len(arr))
	for i := range temp_arr {
		temp_arr[i] = make([]int, len(arr[0]))
	}
	for i := range arr {
		for j := range arr[0] {
			temp_arr[i][j] = arr[i][j]
		}
	}
	return temp_arr
}

//
func display(arr [][]int, colorCode []string, score int) {
	for i := range arr {
		if i == 0 {
			fmt.Print("\r\033[1m  ")
			for k := 0; k < len(arr[0]); k++ {
				fmt.Print("_ ")
			}
			fmt.Println("\033[m")
		}
		fmt.Print("\r\033[1m||\033[m")
		for j := range arr[0] {
			// fmt.Print(arr[i][j]," ")		//for print out 0, 1 (real board)
			if arr[i][j] != 0 {
				fmt.Print(colorCode[arr[i][j]-1] + "[]\033[m")
			} else if arr[i][j] == 0 && i == len(arr)-1 {
				fmt.Print("\033[1m_ \033[m")
			} else {
				fmt.Print("  ")
			}
		}
		if i == 1 {
			fmt.Println("\033[1m||          Your score: ", score, "\033[m")
		} else {
			fmt.Println("\033[1m||\033[m")
		}
	}
	fmt.Println("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
	fmt.Println("\rPress: Spacebar - Hard Drop")
	fmt.Println("\rPress: X or Ctrl + Z to exit the program ")
}

//
func clear_screen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

//
func readKey() string {
	// disable input buffering
	exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
	// do not display entered characters on the screen
	exec.Command("stty", "-F", "/dev/tty", "-echo").Run()
	var b []byte = make([]byte, 1)
	os.Stdin.Read(b)
	return string(b)
}

//
func thread_get_input(input_queue chan string) {
	for {
		key := readKey()
		input_queue <- key
		if key == "x" || key == "X" {
			os.Exit(0)
		}
	}
}

//
func clear_chan(input_queue chan string) chan string {
	for len(input_queue) != 0 {
		<-input_queue
	}
	return input_queue
}

//
func get_key(board [][]int, land [][]int, block Block, input_queue chan string, score int, level_up int) ([][]int, [][]int, Block, chan string, string) {
	var key string
	var pre_sec int = int(time.Now().UnixNano() / 1000000)
	for len(input_queue) != 0 {
		key = <-input_queue
		if key == "w" || key == "W" {
			block.rotate_modify(land)
		} else if key == "a" || key == "A" {
			block.move_left(land)
		} else if key == "d" || key == "D" {
			block.move_right(land)
		} else if key == "s" || key == "S" {
			block.move_down(land)
		} else if key == " " {
			block.hard_drop(land)
		}
		board = make_copy(land)
		board = merge_block(board, block)
		clear_screen()
		display(board, color, score)
		time.Sleep(time.Millisecond)
		//if time pass (1 sec or less depended on level_up var)
		if (int(time.Now().UnixNano()/1000000)-pre_sec >= 1000-level_up) || key == " " {
			break
		}
	}
	return board, land, block, input_queue, key
}

//Drive game
func Tetris() {
	//initializing
	land := init_board()
	board := init_board()
	score := 0
	//score and time drop
	var level_up int = int(160 * int(score/500))
	var pre_sec int = 0
	//
	var key string
	input_queue := make(chan string, 100)
	//begin get key input thread
	go thread_get_input(input_queue)
	//begin the game
	block := rand_block(shape_of_block, board)
	board = merge_block(board, block)
	clear_screen()
	display(board, color, score)
	//
	for {
		level_up = int(160 * int(score/500))
		if score >= 3000 {
			level_up = 900
		}
		//
		board, land, block, input_queue, key = get_key(board, land, block, input_queue, score, level_up)
		if int(time.Now().UnixNano()/1000000)-pre_sec >= 1000-level_up || key == " " {
			if check_bottom(land, block) {
				input_queue = clear_chan(input_queue)
				land = make_copy(board)
				land, score = full_line(land, score)
				//
				block = rand_block(shape_of_block, board)
			} else {
				block.move_down(land)
			}
			board = make_copy(land)
			board = merge_block(board, block)
			clear_screen()
			display(board, color, score)
			pre_sec = int(time.Now().UnixNano() / 1000000)
		}
		if reach_top(land) {
			clear_screen()
			display(land, color, score)
			fmt.Println("\r\033[1;31mGAME OVER\033[m")
			os.Exit(0)
		} else {
			time.Sleep(time.Millisecond)
		}
	}
}

//Drive main
func main() {
	rand.Seed(time.Now().UnixNano()) //in case receiving the same data at a lot of next time
	Tetris()
}
