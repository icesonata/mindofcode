import psycopg2
import tetromino
import os
from libtetris import *
from datetime import datetime
from math import *

#for manage player's data and update data frequently
#attributes: playerid:int, board:string, block:Block(object), nextblock:Block(object) \
#connection:psycopg2.connect():(object)
class Data:
    def __init__(self, playerid, land, block, nextBlock, score):
        self.playerid = playerid    #type int, range [1,4]
        self.score = score
        self.land = land
        self.block = block
        self.nextBlock = nextBlock
        self.connection = self.connectDB()  #make connection to database
        self.blockID, self.nextBlockID = self.getBlockID()

    #reinitialize block and nextBlock and land
    def reinit(self,land,block,nextBlock,score):
        self.score = score
        self.land = land
        self.block = block
        self.nextBlock = nextBlock
        
    #
    def getBlockID(self):
        cursor = self.connection.cursor()
        cursor.execute(f"select * from playerdata where id={self.playerid}")
        #get blockid and nextblockid from database
        _, _, blockid, nextblockid, _, _ = cursor.fetchone()
        cursor.close()
        return blockid, nextblockid

    #static class method converts 2D array to one single line of string
    #input: arr:[][]int
    #output: string
    @staticmethod
    def convertArrayToString(arr):
        strout = ""
        for i in range(len(arr)):
            for j in range(len(arr[0])):
                strout += str(arr[i][j])
        return strout
    
    #convert tuple type to one line string
    @staticmethod
    def convertTupleToString(tup):
        strout = ""
        for i in range(len(tup)):
            if i == len(tup)-1:
                strout += str(tup[i])
            else:
                strout += str(tup[i]) + ","

        return strout

    #update (renew) data on database
    def updateData(self):
        cursor = self.connection.cursor()
        cursor.execute(
            f"update playerdata "
            f"set time=\'{datetime.now()}\', land=\'{self.convertArrayToString(self.land)}\', score={self.score} "
            f"where id={self.playerid};"
        )
        self.connection.commit()
        cursor.execute(
            f"update block "
            f"set x={self.block.x}, y={self.block.y}, shapetype={self.block.shapeType}, rotatetype={self.block.rotateType}, "
            f"shape=\'{self.convertArrayToString(self.block.shape)}\' "
            f"where blockid=\'{self.blockID}\';"
        )
        self.connection.commit()
        cursor.execute(
            f"update nextblock "
            f"set x={self.nextBlock.x}, y={self.nextBlock.y}, shapetype={self.nextBlock.shapeType}, rotatetype={self.nextBlock.rotateType}, "
            f"shape=\'{self.convertArrayToString(self.nextBlock.shape)}\' "
            f"where nextblockid=\'{self.nextBlockID}\';"
        )
        self.connection.commit()
        cursor.close()

    @staticmethod
    #convert string to 2 dimensional array
    #input: string
    def convertStringToArray(data):
        result = []
        temp = []
        subSize = int(sqrt(len(data)))
        for i in range(len(data)):
            temp.append(int(data[i]))
        while len(temp) > subSize:
            sub = temp[:subSize]
            result.append(sub)
            temp = temp[subSize:]
        result.append(temp)

        return result

    @staticmethod
    #convert string to board
    #input: string
    def convertStringToBoard(data):
        result = []
        temp = []
        subSize = 10
        for i in range(len(data)):
            temp.append(int(data[i]))
        while len(temp) > subSize:
            sub = temp[:subSize]
            result.append(sub)
            temp = temp[subSize:]
        result.append(temp)

        return result
            
    #deserialize data received from socket
    #update data received to database and convert to []string for output
    #input: data:string separated with "," character
    #   with index i = {1:playerid, ... , 4:board, ... , 7:block's x, 8:block's y, 9:block's shaptype, 10:block's rotatetype,
    #   11:block's shape, ... , 13:nextblock's x, 14:nextblock's y, 15:nextblock's shapetype, 
    #   16:nextblock's rotatetype, 17:nextblock's shape}
    def deserializeData(self,data):
        data = data.split(",")
        land = self.convertStringToBoard(data[4])
        block = tetromino.Block(int(data[7]),int(data[8]),self.convertStringToArray(data[11]),int(data[9]),int(data[10]))
        nextblock = tetromino.Block(int(data[13]),int(data[14]),self.convertStringToArray(data[17]),int(data[15]),int(data[16]))
        score = int(data[5])
        #call re-initiate method
        self.reinit(land,block,nextblock,score)

    #serialize data for sending
    def serializeData(self):
        cursor = self.connection.cursor()
        cursor.execute(
            f"select * from playerdata "
            f"inner join block on playerdata.blockid = block.blockid "
            f"inner join nextblock on playerdata.nextblockid = nextblock.nextblockid "
            f"where id = {self.playerid};"
        )
        strData = cursor.fetchone()
        strData = self.convertTupleToString(strData)
        cursor.close()
        return strData

    #connecting to database
    @staticmethod
    def connectDB():
        try:
            connection = psycopg2.connect(
                user="hoanglong",
                password="hoanglong",
                host="127.0.0.1",
                port="15433",
                database="tetris"
            )
        except (Exception, psycopg2.Error) as error:
            print(error)
        finally:
            return connection

    #destructor
    #closing database after work
    def __del__ (self):
        self.connection.close()
        print("closed database")

#open database server
def openDB():
    os.system("docker start postgres9.5-alpine")

#close database server
def closeDB():
    os.system("docker stop postgres9.5-alpine")