import tetromino
import recordData
from libsocket import *
from libtetris import *
import time, copy, sys, queue, threading
from datetime import datetime

# based on 2 dimensional space with reversing y-axis
# socket using TCP protocol

#input: color:[]string, shape:[][][]int, inputQueue:Queue(values' type:string), data*:Queue(values' type:string), connSignal:Queue
def Tetris(color,shape,inputQueue,dataRecvQueue,connSignal):
    #Main program begin here:
    #Initialization
    #initialize some necessary temporary variable
    levelUp = 0
    preSec = 0
    key = ""
    tempData = ""
    #initialize block and next block
    block = randomBlock(shape)
    nextBlock = randomBlock(shape)
    #open Database
    print("\rStarting database")
    # recordData.openDB()
    sleep(2)
    #initializing and connecting to database
    data1 = recordData.Data(
        playerid=1,
        land=initBoard(),
        block=randomBlock(shape),
        nextBlock=randomBlock(shape),
        score=0
    ) #player 1 (server)
    data2 = recordData.Data(
        playerid=2,
        land=initBoard(),
        block=randomBlock(shape),
        nextBlock=randomBlock(shape),
        score=0
    )   #player 2 (client)
    #update data1 to database
    data1.updateData()
    #put data of player 1 to dataSendQueue for sending player 1's data to other player
    dataSendQueue.put(data1.serializeData())
    #wait for receiving other player's data from client
    while connSignal.empty():
        clearScreen()
        print("\rWait for other player. . .")
        sleep(0.01)
    #the loop was broken -> successfully receiving data from other player (client)
    #handles received data (re-Ininitialize)
    data2.deserializeData(dataRecvQueue.get())
    data2.updateData()
    #displays beginning of the game
    p1_display = outputConvert(data1.land,data1.block,data1.nextBlock,data1.score,color)
    p2_display = outputConvert(data2.land,data2.block,data2.nextBlock,data2.score,color)
    strout = mergeStringArray(p1_display,p2_display)
    display(strout)
    while True:
        #initiate level of the game
        levelUp = 160 * int(data1.score/500)
        if data1.score >= 3000:
            levelUp = 900
        #do movement according to player's request
        block, inputQueue, key = doMovement(data1.land,data1.block,data1.nextBlock,inputQueue,data1.score,levelUp,color,p2_display)
        #if one second passed or hard-drop was made
        if int(round(time.time()*1000)) - preSec >= 1000 - levelUp or key == " ":
            #block drops down 1 line 
            data1.block.y += 1
            if checkCollision(data1.land,data1.block):
                data1.block.y -= 1
                data1.land = mergeBlock(data1.land,data1.block)
                data1.land, data1.score = handleFullLine(data1.land,data1.score)
                #nextBlock now become block
                data1.block = data1.nextBlock
                #nextBlock get new random Block from function
                data1.nextBlock = randomBlock(shape)
                inputQueue.queue.clear()
            #assign preSec to present time
            preSec = int(round(time.time()*1000))
        #display section
        p1_display = outputConvert(data1.land,data1.block,data1.nextBlock,data1.score,color)
        #get the newest other player's data (player 2) to display (it's like prevent high ping in my opinion)
        while not dataRecvQueue.empty():
            tempData = dataRecvQueue.get()
        #if the loop above was broken -> 'tempData' variable is the newest data
        #make sure that 'tempData' is not just an empty string
        if tempData != "" and tempData != "x":
            data2.deserializeData(tempData)    
        p2_display = outputConvert(data2.land,data2.block,data2.nextBlock,data2.score,color)
        strout = mergeStringArray(p1_display,p2_display)
        display(strout)
        #update data on database
        data1.updateData()
        #put player 1's data to dataSendQueue so that other player (client) can receive the newest data of player 1
        dataSendQueue.put(data1.serializeData())
        if checkReachTop(data1.land) or key == "x" or key == "X":
            dataSendQueue.put("x")
            print("\rGAME OVER")
            print("\rclosing database")
            # recordData.closeDB()
            sleep(2)
            sys.exit()
        else:
            sleep(0.049)

#initialize threads
drive1 = threading.Thread(target=Tetris,args=(colorCode,blockShape,inputQueue,dataRecvQueue,connSignal))
drive2 = threading.Thread(target=getKey)
drive3 = threading.Thread(target=connectSocket,args=(dataSendQueue,))

#start threads
drive1.start()
drive2.start()
drive3.start()