import socket, queue
import recordData

#this code is applied for server (server hosts database also)

#queue to manage data
dataRecvQueue = queue.Queue()   #type string
dataSendQueue = queue.Queue()   #type string

#To check if server-client connect successfully
connSignal = queue.Queue()      #type boolen

#get dynamic external IP
def get_ip_addr():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    addr = s.getsockname()[0]
    s.close()
    return addr

#create connection via socket
#input:
def connectSocket(dataSendQueue):
    #initialize host, port and some temporary variable
    #host: dynamic IP
    #port: default setting as 1622 (configurable)
    host = ''
    port = 1622     #port must be higher than 1023  #chosen by me
    dataRecv = ""
    #connect via TCP
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        conn, addr = s.accept()
        with conn:
            #if connection make perfectly, put TRUE values to queue to announce that connection was successful
            connSignal.put(True)
            #check queue to advoid emptiness make error via sending data
            while dataSendQueue.empty():
                pass    
            #start transmission process
            while True:
                #receiving data from client
                data = conn.recv(2048)
                #send server's data (player 1's data) based on database on server to client (player 2)
                #if dataSendQueue is not empty, send those datas in dataSendQueue
                # if not dataSendQueue.empty():
                conn.sendall(bytes(dataSendQueue.get(),"utf-8"))
                #decode data received from bytes --> string by "utf-8"
                dataRecv = data.decode("utf-8")
                #if received data is not a exit signal character, put received data decoded in dataRecvQueue
                if dataRecv == "x" or dataRecv == "X":
                    break   #break the loop --> end program
                else:
                    dataRecvQueue.put(dataRecv)
