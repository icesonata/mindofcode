import time
import os

#Initialize material
pre_sec = 0
colon = [
    "   \t",
    "   \t",
    " _ \t",
    "|_|\t",
    "   \t",
    " _ \t",
    "|_|\t",
    "   \t"]
zero = [
    " _ _ _ _ _ _ \t",
    "|  _ _ _ _  |\t",
    "| |       | |\t",
    "| |       | |\t",
    "| |       | |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]
one = [
    "           _ \t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          |_|\t"]
two = [
    " _ _ _ _ _ _ \t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    "_ __ _ _ _| |\t",
    "|  _ _ _ _ _|\t",
    "| |          \t",
    "| |_ _ _ _ _ \t",
    "|_ _ _ _ _ _|\t"]
three = [
    " _ _ _ _ _ _ \t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    "     _ _ _| |\t",
    "    |_ _ _  |\t",
    "          | |\t",
    " _ _ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]
four = [
    " _         _ \t",
    "| |       | |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    "          | |\t",
    "          |_|\t"]
five = [
    " _ _ _ _ _ _ \t",
    "|  _ _ _ _ _|\t",
    "| |          \t",
    "| |_ _ _ _ _ \t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    " _ _ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]
six = [
    " _ _ _ _ _ _ \t",
    "|  _ _ _ _ _|\t",
    "| |          \t",
    "| |_ _ _ _ _ \t",
    "|  _ _ _ _  |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]
seven = [
    " _ _ _ _ _ _ \t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          | |\t",
    "          |_|\t"]
eight = [
    " _ _ _ _ _ _ \t",
    "|  _ _ _ _  |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|  _ _ _ _  |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]
nine = [
    " _ _ _ _ _ _ \t",
    "|  _ _ _ _  |\t",
    "| |       | |\t",
    "| |_ _ _ _| |\t",
    "|_ _ _ _ _  |\t",
    "          | |\t",
    " _ _ _ _ _| |\t",
    "|_ _ _ _ _ _|\t"]

#Print out number by char following arguments
def print_num(number,i):
    num = int(number) #the "number" variable is a floating number so we need to convert it into integer to compare
    if num == 0:
        return zero[i]
    elif num == 1:
        return one[i]
    elif num == 2:
        return two[i]
    elif num == 3:
        return three[i]
    elif num == 4:
        return four[i]
    elif num == 5:
        return five[i]
    elif num == 6:
        return six[i]
    elif num == 7:
        return seven[i]
    elif num == 8:
        return eight[i]
    elif num == 9:
        return nine[i]

#print out hour:minute:second
def print_time(h,m,s):
    for i in range(0,len(eight)): #lenght of array eight (=8), another array are the same
        print(print_num((h/10)%10,i),print_num(h%10,i),colon[i],print_num((m/10)%10,i),print_num((m%10),i),colon[i],print_num((s/10)%10,i),print_num(s%10,i))   

#divide 10 and then modulo 10 to print out the first digit in number #just modulo 10 to print out the last digit in number
#example: 12 -> print "1" first and then print "2" -> (12/10)%10 = 1 -> print 1 -> 12%10 = 2 -> print 2

#Drive main
while True:
    from datetime import datetime
    now = datetime.now()
    if now.second != pre_sec:
        os.system('clear')
        print(now)
        print("%s:%s:%s" % (now.hour,now.minute,now.second))
        print_time(now.hour,now.minute,now.second)
        pre_sec = now.second
    time.sleep(0.01)


###Reference print out char perform digital number

# print(" _ _ _ _ _ _ \t _ _ _ _ _ _ \t   \t _ _ _ _ _ _  \t _ _ _ _ _ _ \t   \t _ _ _ _ _ _  \t _ _ _ _ _ _ ")
# print("|  _ _ _ _  |\t|  _ _ _ _  |\t   \t|   _ _ _ _  |\t|  _ _ _ _  |\t   \t|   _ _ _ _  |\t|  _ _ _ _  |")
# print("| |       | |\t| |       | |\t _ \t|  |       | |\t| |       | |\t _ \t|  |       | |\t| |       | |")
# print("| |_ _ _ _| |\t| |_ _ _ _| |\t|_|\t|  |_ _ _ _| |\t| |_ _ _ _| |\t|_|\t|  |_ _ _ _| |\t| |_ _ _ _| |")
# print("|  _ _ _ _  |\t|  _ _ _ _  |\t   \t|   _ _ _ _  |\t|  _ _ _ _  |\t   \t|   _ _ _ _  |\t|  _ _ _ _  |")
# print("| |       | |\t| |       | |\t _ \t|  |       | |\t| |       | |\t _ \t|  |       | |\t| |       | |")
# print("| |_ _ _ _| |\t| |_ _ _ _| |\t|_|\t|  |_ _ _ _| |\t| |_ _ _ _| |\t|_|\t|  |_ _ _ _| |\t| |_ _ _ _| |")
# print("|_ _ _ _ _ _|\t|_ _ _ _ _ _|\t   \t|_ _ _ _ _ _ |\t|_ _ _ _ _ _|\t   \t|_ _ _ _ _ _ |\t|_ _ _ _ _ _|")

# " _ _ _ _ _ _ \t"
# "|  _ _ _ _  |\t"
# "| |       | |\t"
# "| |_ _ _ _| |\t"
# "|  _ _ _ _  |\t"
# "| |       | |\t"
# "| |_ _ _ _| |\t"
# "|_ _ _ _ _ _|\t"
