import random, copy
from libtetris import *

#initialize block as an Object (including elements: coordinate 'x':int, coordinate 'y':int, shape of block:[][]int)
#all element of object 'Block' are public, which means you can call it everytime you want
#it will be easier to work when using with coordinates
class Block(object):
    def __init__(self,x,y,shape,shapeType,rotateType):
        self.x = x              #storing horizontal coordinate x
        self.y = y              #storing vertical coordinate y
        self.shape = shape[:]   #shape[][]:int - storing shape of block
        self.shapeType = shapeType  #store following index of blockShape (range [0,6] means shapeO->shapeT above). For rotation supporting
        self.rotateType = rotateType    #store 1 number in range [0,3]. For rotation supporting
    #change all value 1 to another value in range [1,7] in order to get various colorful block 
    def reInit(self):
        num = random.randint(1,7)
        for i in range(len(self.shape)):
            for j in range(len(self.shape[0])):
                if self.shape[i][j] != 0:
                    self.shape[i][j] = num
    #transpose 2-Dimensional (square) matrix
    def transpose(self):
        for i in range(len(self.shape)): 
            for j in range(i+1,len(self.shape[0])):
                self.shape[i][j], self.shape[j][i] = self.shape[j][i], self.shape[i][j]
    #reverse (swap) each layer column of 2D matrix
    #input: arr[][]:int
    #output: arr[][]:int
    #reference: https://stackoverflow.com/questions/42519/how-do-you-rotate-a-two-dimensional-array/8664879#8664879?newreg=e1f2cab8974b45dcbe5ac61e8d1be2c9
    def reverseColumn(self):
        for i in range(len(self.shape)):
            for j in range(int(len(self.shape[0])/2)):
                self.shape[i][j], self.shape[i][len(self.shape[0])-1-j] = self.shape[i][len(self.shape[0])-1-j], self.shape[i][j]
    #rotate block
    def rotate(self,land):
        previousShape = copy.deepcopy(self.shape)
        self.transpose()
        self.reverseColumn()
        tempX = self.x
        tempY = self.y
        flag = False
        #if collision happen after the rotation
        #do all possible test in caseX array
        #X is number value in range [0,3] depends on rotateType, which shows transformation of block e.g 0->R, R->2)
        if checkCollision(land,self):
            #if block is not I Tetromino
            if self.shapeType != 5:
                if self.rotateType == 0:    #0->R
                    for i in range(len(case1)):
                        self.x += case1[i][0]
                        self.y += case1[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                elif self.rotateType == 1:  #R->2
                    for i in range(len(case2)):
                        self.x += case2[i][0]
                        self.y += case2[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                elif self.rotateType == 2:  #2->L
                    for i in range(len(case3)):
                        self.x += case3[i][0]
                        self.y += case3[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                else:   #== 3   #L->0
                    for i in range(len(case4)):
                        self.x += case4[i][0]
                        self.y += case4[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
            #if block is I Tetromino
            else:   #case shape I
                if self.rotateType == 0:    #0->R
                    for i in range(len(case1_i)):
                        self.x += case1_i[i][0]
                        self.y += case1_i[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                elif self.rotateType == 1:  #R->2
                    for i in range(len(case2_i)):
                        self.x += case2_i[i][0]
                        self.y += case2_i[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                elif self.rotateType == 2:  #2->L
                    for i in range(len(case3_i)):
                        self.x += case3_i[i][0]
                        self.y += case3_i[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
                else:   #== 3   #L->0
                    for i in range(len(case4_i)):
                        self.x += case4_i[i][0]
                        self.y += case4_i[i][0]
                        if not checkCollision(land,self):
                            flag = True
                            break
                        else:
                            self.x = tempX
                            self.y = tempY
        else:
            flag = True
        #check if rotate was succeed or not
        if flag == False:   #rotation failed
            self.shape = previousShape
        else:               #rotation succeed
            #if rotateType's value is 3 (max is 3), reset its value to 0
            if self.rotateType == 3:
                self.rotateType = 0
            #else increase rotateType's value
            else:
                self.rotateType += 1
        
    def moveDown(self,land):
        self.y += 1
        if checkCollision(land,self):
            self.y -= 1

    def moveLeft(self,land):
        self.x -= 1
        if checkCollision(land,self):
            self.x += 1

    def moveRight(self,land):
        self.x += 1
        if checkCollision(land,self):
            self.x -= 1

    def hardDrop(self,land):
        while not checkCollision(land,self):
            self.y += 1
        #we make it into not checkCollision after force to drop it until collision happen
        self.y -= 1