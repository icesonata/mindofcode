# # create font of characters:

num_0 = [" _ _  ",
         "|   | ",
         "|_ _| "]
num_1 = ["   ",
         "/| ",
         " | "]
num_2 = [" _ _  ",
         " _ _| ",
         "|_ _  "]
num_3 = ["_ _  ",
         "_ _| ",
         "_ _| "]
num_4 = ["      ",
         "|_ _| ",
         "    | "]
num_5 = [" _ _  ",
         "|_ _  ",
         " _ _| "]
num_6 = [" _ _  ",
         "|_ _  ",
         "|_ _| "]
num_7 = ["_ _ ",
         "  / ",
         " /  "]
num_8 = [" _ _  ",
         "|_ _| ",
         "|_ _| "]
num_9 = [" _ _  ",
         "|_ _| ",
         " _ _| "]
symbol = ["    ",
          " *  ",
          " *  "]

font = [num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, symbol]

# # in: datetime of OS
# # out: string "07:35:43"
 def read_system_clock()
    time = Time.new 
    full_time_str = time.to_s
    hhmmss = full_time_str[11..18]
    return hhmmss
 end

# # in: string "07:35:43"
# #     font
# # out: [ [], [], [] ...]
def convert_characters_to_fonts(time_str, font)
    out = []
    for i in (0...time_str.length())
        character = time_str[i]
        converted_font = convert_1_character_to_1_font(character, font)
        out.append(converted_font)
    end
    return out
end

def merge_2_font(font1, font2)
    font = [""] * font1.length()

    for i in (0...font1.length())
        font[i] = font1[i]+font2[i]
    end
    return font
end
# # int: 
# # out: ["row_1", "row_2", ... ]
def merge_array_of_fonts(arr_of_fonts)
    number_of_row = arr_of_fonts[0].length()

    merged_font = [""]*number_of_row

    for i in (0...arr_of_fonts.length())
        merged_font = merge_2_font(merged_font, arr_of_fonts[i])
    end
    return merged_font
end

# # int: character & font
# # out: [] tuong ung voi 1 font
def convert_1_character_to_1_font(character, font)
    if character == "0"
        return font[0]
    elsif character == "1"
        return font[1]
    elsif character == "2"
        return font[2]
    elsif character == "3"
        return font[3]
    elsif character == "4"
        return font[4]
    elsif character == "5"
        return font[5]
    elsif character == "6"
        return font[6]
    elsif character == "7"
        return font[7]
    elsif character == "8"
        return font[8]
    elsif character == "9"
        return font[9]
    elsif character == ":"
        return font[10]
    end
end

def display_arr(arr)
    for i in (0...arr.length())
        row_i = arr[i]
        puts row_i
    end
end

def clear_screen()
    system("clear")
end

def delay(sleep_time)
    sleep(sleep_time)
end

$pre_sec = 0
while TRUE
    time_str = read_system_clock()
    if $pre_sec != time_str[7..8].to_i
        clear_screen()
        puts time_str
        converted = convert_characters_to_fonts(time_str, font)
        merged = merge_array_of_fonts(converted)

        display_arr(merged)
        $pre_sec = time_str[7..8].to_i
    end
    delay(0.01)
end