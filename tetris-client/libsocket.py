import socket, queue
import recordData

#this is applied for client

#initialize queues manage data
dataRecvQueue = queue.Queue()   #type string
dataSendQueue = queue.Queue()   #type string

#check connection whether succeed or not
connSignal = queue.Queue()  #type boolen

#get real IPv4 address
def get_ip_addr():
    conn = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    conn.connect(("8.8.8.8",80))
    #return IPv4 address of this computer and address
    addr = s.getsockname()[0]
    conn.close()
    return addr

#create connection via socket
#input:
def connectSocket(dataSendQueue):
    #initialize host, port and some temporary variable
    host = ''
    port = 1622 #port must be higher than 1023  #chosen by me
    dataRecv = ""
    # #connect via TCP
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        connSignal.put(True)
        #check queue to advoid emptiness make error via sending data
        while dataSendQueue.empty():
            pass
        #if connection was successful, starts transmission process
        while True:
            #if dataSendQueue is not empty, send those datas in dataSendQueue
            # if not dataSendQueue.empty():
            s.sendall(bytes(dataSendQueue.get(),"utf-8"))
            #receiving data from server
            data = s.recv(2048)
            #decode received data
            dataRecv = data.decode("utf-8")
            #check if dataRecv is 'exit' signal
            if dataRecv == "x" or dataRecv == "X":
                dataRecvQueue.put("x")
                break
            else:   #put dataRecv in dataRecvQueue
                dataRecvQueue.put(dataRecv)
            
            

