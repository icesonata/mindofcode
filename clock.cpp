#include <iostream>
#include <ctime>    
#include <unistd.h> //usleep

using namespace std;

string num_0[3] = {
	" _ _  ",
    "|   | ",
    "|_ _| "};
string num_1[3] = {
	"   ",
    "/| ",
	" | "};
string num_2[3] = {
	" _ _  ",
    " _ _| ",
    "|_ _  "};
string num_3[3] = {
	"_ _  ",
	"_ _| ",
    "_ _| "};
string num_4[3] = {
	"      ",
    "|_ _| ",
    "    | "};
string num_5[3] = {
	" _ _  ",
	"|_ _  ",
    " _ _| "};
string num_6[3] = {
	" _ _  ",
    "|_ _  ",
    "|_ _| "};
string num_7[3] = {
	"_ _ ",
    "  / ",
    " /  "};
string num_8[3] = {
	" _ _  ",
    "|_ _| ",
    "|_ _| "};
string num_9[3] = {
	" _ _  ",
    "|_ _| ",
    " _ _| "};
string symbol[3] = {
	"    ",
    " *  ",
    " *  "};

string** init_font();
void print_out(string* merged_str);
string current_time();
string** convert_string_to_font(string time_str,string** font);
string* convert_1_char_to_1_font(char t,string** font);
string* merge_array_of_font(string** out);
string get_substr(string str,int begin,int end);

int main()
{
	string** font; //2d array store numbers (as characters)
	string** out; //2d array for result storage
	string* str_out; //initialize string out as a pointer
	font = init_font();
	int pre_sec = 0;
	string time_;
	int time_sec;
	while(true)
	{
		time_ = get_substr(current_time(),11,18); //get substring from index 11 to 18
		time_sec = atoi(get_substr(time_,6,7).c_str()); //converts string to integer in order to get second as int
		if(pre_sec != time_sec)
		{
			cout << "\033[2J\033[1;1H"; //clear terminal
			cout<<time_<<endl; //print out hh:mm:ss
			out = convert_string_to_font(time_,font);
			str_out = merge_array_of_font(out); //get pointer (array pointer) to print out
			print_out(str_out);
		//free memories's space to avoid wasting memories and segmentation fault
			delete[] str_out; 
			for(int i=0;i<8;i++)
			{
				delete[] out[i];
			}
			delete[] out;
		// " " . . .
			pre_sec = time_sec;
		}
		usleep(1000); //1000 in microsecond = 1 millisecond
	}
	return 0;
}

//intializing
string** init_font()
{
	string** font = new string*[11];
	//initialize font[i] with i:0 to 10 (font[0],font[1],...,font[10])
	for(int i=0; i < 11; i++)
	{
		font[i] = new string[3];
	}

	for(int i=0; i < 3;i++)
	{
		font[0][i] = num_0[i];
		font[1][i] = num_1[i];
		font[2][i] = num_2[i];
		font[3][i] = num_3[i];
		font[4][i] = num_4[i];
		font[5][i] = num_5[i];
		font[6][i] = num_6[i];
		font[7][i] = num_7[i];
		font[8][i] = num_8[i];
		font[9][i] = num_9[i];
		font[10][i] = symbol[i];
	}
	return font;
}

//get substring from index begin to end
//in: string
//out: substring
string get_substr(string str,int begin,int end)
{
	string str_out = "";
	for(int i=0;i<=(end-begin);i++)
	{
		str_out += str[begin+i];
	}
	return str_out;
}

//get current date time
//output: type string
string current_time()
{
	time_t t = time(0);
	string dt = ctime(&t);
	return dt;
}

//print out result array
void print_out(string* merged_str)
{
	for(int i=0; i < 3;i++)
	{
		cout<<merged_str[i]<<endl;
	}
}

//in: string, string** (2D array)
//out: string** - 2d array [[],[],[],]
string** convert_string_to_font(string time_str,string** font)
{
	string* index_; //temporary pointer for storage
	int k = 0;
	string** out = new string*[8]; //initialize 2D pointer
	//initialize for font 2D array
	for(int i=0; i < 8;i++)
	{
		out[i] = new string[3];
		for(int j=0; j < 3;j++)
		{
			out[i][j] = "";
		}
	}
	//
	for(int i=0; i < time_str.length(); i++)
	{
		index_ = convert_1_char_to_1_font(time_str[i],font);
		for(int j=0; j < 3; j++)
		{
			out[k][j] = index_[j];
		}
		k++;
	}
	return out; 
}

//get correcting font array
//input: char, string** (2D array)
//output: string* (1D array)
string* convert_1_char_to_1_font(char t,string** font)
{
	if(t=='0') return font[0];
	if(t=='1') return font[1];
	if(t=='2') return font[2];
	if(t=='3') return font[3];
	if(t=='4') return font[4];
	if(t=='5') return font[5];
	if(t=='6') return font[6];
	if(t=='7') return font[7];
	if(t=='8') return font[8];
	if(t=='9') return font[9];
	if(t==':') return font[10];
	return nullptr;
}

// int: [[],[],[],...]
// out: ["row_1", "row_2", ... ] - 1d array being a pointer
string* merge_array_of_font(string** out)
{
	string* str_out = new string[3];
	for(int i=0;i<8;i++)
	{
		for(int j=0;j<3;j++)
		{
			str_out[j] += out[i][j];
		}
	}
	return str_out;
}
