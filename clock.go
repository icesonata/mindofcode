package main

import (
	"fmt"
	"os"
	"os/exec"
	"time"
	"strconv"
)

// create font of characters:

var num_0 = []string{
	" _ _  ",
	"|   | ",
	"|_ _| "}
var num_1 = []string{
	"   ",
	"/| ",
	" | "}
var num_2 = []string{
	" _ _  ",
	" _ _| ",
	"|_ _  "}
var num_3 = []string{
	"_ _  ",
	"_ _| ",
	"_ _| "}
var num_4 = []string{
	"      ",
	"|_ _| ",
	"    | "}
var num_5 = []string{
	" _ _  ",
	"|_ _  ",
	" _ _| "}
var num_6 = []string{
	" _ _  ",
	"|_ _  ",
	"|_ _| "}
var num_7 = []string{
	"_ _ ",
	"  / ",
	" /  "}
var num_8 = []string{
	" _ _  ",
	"|_ _| ",
	"|_ _| "}
var num_9 = []string{
	" _ _  ",
	"|_ _| ",
	" _ _| "}
var symbol = []string{
	"    ",
	" *  ",
	" *  "}

var font = [][]string{num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, symbol}

// in: datetime of OS
// out: string "07:35:43"
func read_system_clock() string {
	full_time_str := time.Now()
	return full_time_str.String()[11:19]
}

// in: string "07:35:43"
//     font
// out: [ [], [], [] ...] - 2d array
func convert_characters_to_fonts(time_str string, font [][]string) [][]string {
	var out [][]string
	var converted_font []string

	for _,str := range time_str {
		converted_font = convert_1_character_to_1_font(string(str), font)
		out = append(out, converted_font)
	}
	return out
}

//in: 2 string font
//out: 1d array carry combined string between font1 & font2

func merge_2_font(font1 []string, font2 []string) []string {
	var font []string

	for i,_ := range font1 {
		font = append(font,font1[i] + font2[i])
	}
	return font
}

// int: [][]string - 2d array char of converted number 
// out: ["row_1", "row_2", ... ] - 1d array
func merge_array_of_fonts(arr_of_fonts [][]string) []string {

	var merged_font []string
	for range arr_of_fonts[0] {
		merged_font = append(merged_font, "")
	}

	for _,item := range arr_of_fonts {
		merged_font = merge_2_font(merged_font, item)
	}
	return merged_font
}

// in: character & fnt
// out: array carry number converted to char
func convert_1_character_to_1_font(character string, font [][]string) []string {
	if character == "0" {
		return font[0]
	}
	if character == "1" {
		return font[1]
	}
	if character == "2" {
		return font[2]
	}
	if character == "3" {
		return font[3]
	}
	if character == "4" {
		return font[4]
	}
	if character == "5" {
		return font[5]
	}
	if character == "6" {
		return font[6]
	}
	if character == "7" {
		return font[7]
	}
	if character == "8" {
		return font[8]
	}
	if character == "9" {
		return font[9]
	}
	if character == ":" {
		return font[10]
	} else {
		return nil
	}
}

//input: string
//output:

func display_arr(arr []string) {
	for _, item := range arr{
		fmt.Println(item)
	}
}

func clear_screen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
//input: string
//out: integer
//convert from string to int with fault check
func int_convert(str string) int {
	i, err := strconv.Atoi(str)
    if err != nil {
        // handle error
        fmt.Println(err)
        os.Exit(-1)
	}
	return i
}

func main() {
	var time_str string
	var converted [][]string
	var merged []string
	var time_second int
	var pre_sec int = 0
	for {
		time_str = read_system_clock()
		time_second = int_convert(time_str[7:8])
		if pre_sec != time_second {
			clear_screen()

			fmt.Println(time.Now())
			fmt.Println(time_str)
			converted = convert_characters_to_fonts(time_str, font)
			merged = merge_array_of_fonts(converted)

			display_arr(merged)
			pre_sec = time_second
		}
		time.Sleep(1 *time.Millisecond )
	}
}
