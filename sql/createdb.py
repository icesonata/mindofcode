import psycopg2

try:
    connection = psycopg2.connect(
        user="hoanglong",
        password="hoanglong",
        host="127.0.0.1",
        port="15433",
        database="longvip")
    cursor = connection.cursor()
    print(connection.get_dsn_parameters(),"\n")
    cursor.execute("Select version();")
    record = cursor.fetchone()
    print("You are connected to - ",record,"\n")
    cursor.execute(
        "insert into memberinfo(id,name,address,city)"+
        "values('4','ke','district 12','hcmc');"+
        "insert into memberinfo(id,name,address,city)"+
        "values('5','long','district 12','hcmc');")
    connection.commit()
    countChange = cursor.rowcount
    print("INSERTED",countChange,"\n")
    cursor.execute("select * from memberinfo;")
    while True:
        record = cursor.fetchone()
        if record==None:
            break
        print(record,"\n")
except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL",error)
finally:
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
