import os, time, random, datetime, readchar, copy, sys, queue, threading
from datetime import datetime

#program requires pip and readchar library from pip

#init board's size
col_size = 10
row_size = 20

#init global array as square matrix
shape_o = [
    [1,1],
    [1,1],
]
shape_l = [
    [1,0,0],
    [1,0,0],
    [1,1,0]
]
shape_j = [
    [0,1,0],
    [0,1,0],
    [1,1,0]
]
shape_s = [
    [1,0,0],
    [1,1,0],
    [0,1,0]
]
shape_z = [
    [1,1,0],
    [0,1,1],
    [0,0,0]
]
shape_i = [
    [0,0,0,0],
    [1,1,1,1],
    [0,0,0,0],
    [0,0,0,0]
]
shape_t = [
    [0,1,0],
    [1,1,1],
    [0,0,0]
]

shape_of_block = [shape_i,shape_j,shape_l,shape_o,shape_s,shape_t,shape_z]

#initialize block as an Object (including elements: coordinate 'x':int, coordinate 'y':int, shape of block:[][]int)
#all element of object 'Block' are public, which means you can call it everytime you want
#it will be easier to work when using with coordinates
class Block(object):
    def __init__(self,x,y,shape):
        self.x = x              #storing horizontal coordinate x
        self.y = y              #storing vertical coordinate y
        self.shape = shape[:]   #shape[][]:int - storing shape of block

#initialize board (new board)
#be careful with initializing 2-Dimensional array
#input:
#output: board[][]:int
def init_board():
    board = []
    for i in range(row_size):
        temp = []
        for j in range(col_size):
            temp.append(0)
        board.append(temp)
    return board

#get random block
#input: shape[][][]:int (storing all shape of block), board[][]:int
#output: block:Block(object)
def rand_block(shape,board):
    rand_shape = copy.deepcopy(random.choice(shape))
    return Block(int(len(board[0])/2) - len(rand_shape[0]) + 1, 0, rand_shape)      #block will appear at the middle of the top of the board

#merge block with board (temporary board)
#input: board[][]:int, block:Block(object)
#output: board[][]:int
def merge_block(board,block):
    for i in range(len(block.shape)):
        for j in range(len(block.shape[0])):
            if block.shape[i][j] == 1:
                board[block.y+i][block.x+j] = 1
    return board

#we will delete unnecessary part of block (columns (col) and rows of block's shape)
#benifit: easier to merge and reduce 'if' function
#input: shape[][]:int
#output: shape[][]:int
def cut_shape(shape):
    row = []
    col = []
    #check rows
    for i in range(len(shape)):
        flag = True
        for j in range(len(shape[0])):
            if shape[i][j] == 1:
                flag = False
        if flag == True:
            row.append(i)
    
    #check columns
    for i in range(len(shape[0])):
        flag = True
        for j in range(len(shape)):
            if shape[j][i] == 1:
                flag = False
        if flag == True:
            col.append(i)
    
    #cut col first
    for i in range(len(col)):
        for j in range(len(shape)): #row j'th of shape
            temp = col[len(col)-1-i]
            shape[j].pop(temp)

    #cut row then
    for i in range(len(row)):
        shape.pop(row[len(row)-1-i])
    
    return shape

#collision between block and land (main board), between block and pieces of other previous block
#input: land[][]:int, block:Block(object)
#output: True or False
def check_collision(land,block):
    #if block's size is over land's size
    if block.x < 0 or block.x + len(block.shape[0]) - 1 >= col_size or block.y+len(block.shape)-1 >= row_size:
        return True
    #check whether block collises with other pieces in land (board) or not
    for i in range(len(block.shape)):
        for j in range(len(block.shape[0])):
            if block.shape[i][j] == 1 and land[block.y+i][block.x+j] == 1:
                return True
    return False

#check whether block reach bottom of land (main board) or not
#checking following each column of block.shape
#the most important function
#input: land[][]:int, block:Block(object)
#output: True or False
def check_bottom(land,block):
    #in case bottom of block reach bottom of land 
    if block.y + len(block.shape) - 1 >= row_size - 1:
        return True
    #else block.y + len(block.shape) - 1 < row_size - 1
    for i in range(len(block.shape[0])):    #block's horizontal dimension
        for j in range(len(block.shape)):   #block's vertical dimension
            flag = True
            #if piece block.shape[j][i] is equal to 0
            if block.shape[j][i] == 0:
                #check all elements in column i
                for k in range(j,len(block.shape)):
                    #if there is any block.shape[k][i] == 1 after block.shape[j][i] == 0
                    #using for case shape_t, and more
                    if block.shape[k][i] == 1:
                        flag = False
                        break
                #if block.shape[j][i] == 0 and all elements left in column i is equal to 0 (block.shape[j...len(block.shape)-1][i] == 0)
                if flag == True:
                    #at that position, if element of board is equal to 1 (land[block.y+j][block.x+i] == 1) and above block.shape[j][i] is equal to 1
                    if land[block.y+j][block.x+i] == 1 and block.shape[j-1][i] == 1:
                        return True
            #check under outside of the block
            if j == len(block.shape) - 1 and block.shape[j][i] == 1:
                if land[block.y+j+1][block.x+i] == 1:
                    return True

    #if the block still in the air and no collision of the under happen, then the block will keep moving
    return False

#reach top??? we just need to check the first line at the top of the land
#if it has any piece, return True to alarm that the game will be over at the next move 
#input: land[][]:int
#output: True or False
def reach_top(land):
    for i in range(len(land[0])):
        if land[0][i] == 1:
            return True         #the game will be over
    return False                #the game is continuing

#falling down
#input: board[][]:int, block:Block(object)
#output: board[][]:int, block:Block(object)
def move_down(board,block):
    if check_bottom(board,block):
        return board, block
    block.y += 1
    if check_collision(board,block):
        block.y -= 1 
        return board, block
    else:
        return board, block
        
#check every single line of land array (main board)
#we will delete lines which is full by make it clear with 0 and swap very couple lines from i to 0 as (line[i] = line[i-1]), (line[i-1] = line[i-2]), ...
#swap from under line to top line 
#input: land[][]:int, score:int
#output: land[][]:int (after checking and re-mergin), score:int 
def check_full_line(land,score):
    flag = False
    line = []
    #checking every single line i which whether are full or not, then store all i in line[]
    for i in range(len(land)):
        flag = False
        for j in range(len(land[0])):
            if land[i][j] == 0:
                break
            if j == len(land[0]) - 1:
               flag = True
        if flag == True:
            score += 100            #get score
            line.append(i)
    #swap every couple lines
    for i in line:
        for j in range(len(land[0])):
            land[i][j] = 0
        temp = copy.deepcopy(land[i])
        land.pop(i)
        land.insert(0,temp)
    
    return land, score
            
#transpose 2-Dimensional (square) matrix
#input: arr[][]:int
#output: arr[][]:int
def transpose(arr):
    for i in range(0,len(arr)): #i means row, j means column
        for j in range(0+i,len(arr[0])): #be careful with range(0+i,len(arr)) #the "+i" is super important
            temp = arr[i][j]
            arr[i][j] = arr[j][i]
            arr[j][i] = temp
    return arr


#reverse (swap) each layer column of 2D matrix
#input: arr[][]:int
#output: arr[][]:int
#reference: https://stackoverflow.com/questions/42519/how-do-you-rotate-a-two-dimensional-array/8664879#8664879?newreg=e1f2cab8974b45dcbe5ac61e8d1be2c9
def reverse_column(arr):
    for i in range(0,len(arr)):
        for j in range(0,int(len(arr[0])/2)):
            temp = arr[i][j]
            arr[i][j] = arr[i][len(arr)-1-j]
            arr[i][len(arr)-1-j] = temp
    return arr

#rotate block (2D array)
#reference: https://stackoverflow.com/questions/42519/how-do-you-rotate-a-two-dimensional-array/8664879#8664879?newreg=e1f2cab8974b45dcbe5ac61e8d1be2c9
#using Method 1 at Rotated by -90 in the reference
#input: arr[][]:int
#output: arr[][]:int
def rotate(shape):
    shape = transpose(shape)
    shape = reverse_column(shape)
    return shape

#check around block (support rotation)
#checking if
#input: land[][]:int, temp_block:Block(object) - origin block's shape
#output: True or False
def check_around_block(land,temp_block):
    for i in range(len(temp_block.shape)):
        for j in range(len(temp_block.shape[0])):
            if land[temp_block.y+i][temp_block.x+j] == 1:
                return False
    return True

#print out 2D array and score
#input: arr[][]:int, score:int
def display(arr,score):
    for i in range(len(arr)):
        if i == 0:
            print("\r  ", end = "")
            for k in range(len(arr[0])):
                print("_ ",end = "")
            print("")
        print("\r||",end ="")
        for j in range(len(arr[0])):
            if arr[i][j]==1:
                print("\033[0;37;47m 1\033[0m",end = "")
            elif arr[i][j]==0 and i == len(arr)-1:
                print("_",end=" ")
            else:
                print(" ", end = " ")
            # print(arr[i][j],end =" ")     #this line of code is to show full 0 & 1 of board (true board) #optional #you can discomment this line and delete lines 282->284 to show out 
        if i == 1:
            print("||          Your score: ",score)         #set where to print out score
        else:
            print("||")           #important to add '\r' when using thread due to the pointer of every single thread (threads' sequence)

#delay program for t second
#input: t:int
def sleep(t):
    time.sleep(t)

#clear console
def clear_screen():
    os.system("clear")

#get keyboard input
def get_input():
    while True:
        key = readchar.readkey()
        input_queue.put(key)
        if key == "x":
            sys.exit()

#Main program begin here:
#Initialization
land = init_board()         #main board
board = init_board()        #temporary board
block = rand_block(shape_of_block,board)            #main block
temp_block = Block(copy.deepcopy(block.x),copy.deepcopy(block.y),copy.deepcopy(block.shape))    #origin block (without being cut)
input_queue = queue.Queue()         #queue contains keyboard input

#begin the game, print out the first scene
clear_screen()  #clear terminal/command line screen to look nicer
block.shape = cut_shape(block.shape)
block.x = int(len(board[0])/2) - len(block.shape[0]) + 1
board = merge_block(board,block)
display(board,0)
print("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
print("\rPress: Spacebar - Hard Drop")
print("\rPress: X and Ctrl + Z to exit the program ")

#do keyboard inputs requestt
#input: board[][]:int, land[][]:int, block:Block(object), temp_block:Block(object), input_queue:Queue(object), score:int, level_up:int
#output: board[][]:int, land[][]:int, block:Block(object), temp_block:Block(object), input_queue:Queue(object)
def get_key(board,land,block,temp_block,intput_queue,score,level_up):
    pre_sec = int(round(time.time() * 1000))
    while not input_queue.empty():      #no keyboard input ? ===> no change
        key = input_queue.get()
        #check size of origin form of block with land
        if key == "w" and temp_block.y + len(temp_block.shape) - 1 <= row_size - 1 and temp_block.x + len(temp_block.shape[0]) - 1 <= col_size - 1 and check_around_block(land,temp_block):
            block.shape = copy.deepcopy(temp_block.shape)
            block.shape = rotate(block.shape)
            temp_block.shape = copy.deepcopy(block.shape)
            block.shape = cut_shape(block.shape)
            #check after change form (change position also)
            if check_collision(land,block):
                block.shape = rotate(block.shape)
                temp_block.shape = copy.deepcopy(block.shape)
                block.shape = cut_shape(block.shape)
        #move left
        if key == "a" and block.x > 0:
            block.x -= 1
            #check after change the position
            if check_collision(land,block):
                block.x += 1
        #move right
        if key == "d" and block.x + len(block.shape[0]) - 1 < col_size - 1:
            block.x += 1
            #check after change the position
            if check_collision(land,block):
                block.x -= 1
        #move down (fall down)
        if key == "s" and not check_bottom(land,block):
            block.y += 1
        #hard drop
        if key == " ":
            #falling down until meet bottom of board
            while not check_bottom(land,block):
                block.y += 1
            #we merge block with temporary board and print it out so that we can end the loop to get new block in next movement
            board = copy.deepcopy(land)
            board = merge_block(board,block)
            clear_screen()
            display(board,score)
            print("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
            print("\rPress: Spacebar - Hard Drop")
            print("\rPress: X and Ctrl + Z to exit the program ")
            #
            break
        #temp block's coordinate change due to block's changing (main block)
        temp_block.x = block.x
        temp_block.y = block.y
        board = copy.deepcopy(land)
        #re-merge after change
        board = merge_block(board,block)
        clear_screen()
        display(board,score)
        print("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
        print("\rPress: Spacebar - Hard Drop")
        print("\rPress: X and Ctrl + Z to exit the program ")
        if int(round(time.time() * 1000)) - pre_sec >= 1000 - level_up:            #if one second pass, end every changes made by keyboard inputs FOR FALL DOWN
            break
    return board, land, block, temp_block, input_queue

#thread 1
#input: board[][]:int, land[][]:int, block:Block(object), temp_block:Block(object), input_queue:Queue(object)
def Tetris(board,land,block,temp_block,input_queue):
    pre_sec = int(round(time.time() * 1000))
    #variable storing player's score
    score = 0
    while True:
        #level_up variable for increase speed of the game after some score achieved
        level_up = 160 * int(score/500)
        if score >= 3000:
            level_up = 900          #max speed of the game will be 1 movement per 0,2 second
        #do the changes in one second according to keyboard inputs stored in queue
        board,land,block,temp_block,input_queue = get_key(board,land,block,temp_block,input_queue,score,level_up)
        #if one second pass, make a next move
        if int(round(time.time()*1000)) - pre_sec >= 1000 - level_up:
            #
            if check_bottom(land,block) == True:
                input_queue.queue.clear()
                land = copy.deepcopy(board)
                land,score = check_full_line(land,score)
                del block
                block = rand_block(shape_of_block,board)
                temp_block.shape = copy.deepcopy(block.shape)
                temp_block.x = block.x
                temp_block.y = block.y
                block.shape = cut_shape(block.shape)
                block.x = int(len(board[0])/2) - len(block.shape[0]) + 1
                board = merge_block(board,block)
            else:
                board = copy.deepcopy(land) #
                board,block = move_down(board,block)
                board = merge_block(board,block)
            #re-locate temp block's coordinate
            temp_block.x = block.x
            temp_block.y = block.y
            #
            clear_screen()
            display(board,score)
            print("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
            print("\rPress: Spacebar - Hard Drop")
            print("\rPress: X and Ctrl + Z to exit the program ")
            pre_sec = int(round(time.time() * 1000))
        #game will be over if any piece of blocks reaches the roof of land
        if reach_top(land):
            print("\rGAME OVER")
            sys.exit()
        else:
            continue
        sleep(0.05)

#initialize threads
drive1 = threading.Thread(target=Tetris,args=(board,land,block,temp_block,input_queue))
drive2 = threading.Thread(target=get_input)
#start the program
drive1.start()
drive2.start()