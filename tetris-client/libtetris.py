import tetromino
import recordData
import os, time, random, readchar, copy, sys, queue, socket
from datetime import datetime

#init board's size
colSize = 10
rowSize = 20

#queue to manage input
inputQueue = queue.Queue()

#init global array as square matrix, we will cut unneccesary parts of matrix (i.e row or column with fully '0') later
shapeO = [
    [1,1],
    [1,1]
]
shapeL = [
    [0,0,1],
    [1,1,1],
    [0,0,0]
]
shapeJ = [
    [1,0,0],
    [1,1,1],
    [0,0,0]
]
shapeS = [
    [0,1,1],
    [1,1,0],
    [0,0,0]
]
shapeZ = [
    [1,1,0],
    [0,1,1],
    [0,0,0]
]
shapeI = [
    [0,0,0,0],
    [1,1,1,1],
    [0,0,0,0],
    [0,0,0,0]
]
shapeT = [
    [0,1,0],
    [1,1,1],
    [0,0,0]
]

#initialize 3-Dimensional array contain various type of block's shape
blockShape = [shapeO,shapeL,shapeJ,shapeS,shapeZ,shapeI,shapeT]

#initialize color code array for display colorful block
#we don't use black color due to black theme Terminal are
colorCode = [
	"\033[0m  \033[m",          #blank
	"\033[0;31;41m[]\033[m",    #Text: Red, Background: Red
	"\033[0;32;42m[]\033[m",    #Text: Green, Background: Green
	"\033[0;33;43m[]\033[m",    #Text: Yellow, Background: Yellow
	"\033[0;34;44m[]\033[m",    #Text: Blue, Background: Blue
	"\033[0;35;45m[]\033[m",    #Text: Purple, Background: Purple
	"\033[0;36;46m[]\033[m",    #Text: Cyan, Background: Cyan
	"\033[0;37;47m[]\033[m"     #Text: White, Background: White
]

#SRS rotation
#Wall Kick case
#apply for J,L,Z,S,T Tetromino's case following clockwise direction
#test1 will be all {0,0} so we will skip test1
case1 = [[-1, 0], [-1, 1], [0, -2], [-1, -2]] #0->R     #R means Right
case2 = [[1, 0], [1, -1], [0, 2], [1, 2]]     #R->2     #2 means succeed 2nd rotation
case3 = [[1, 0], [1, 1], [0, -2], [1, -2]]    #2->L     #L means Left
case4 = [[-1, 0], [-1, -1], [0, 2], [-1, 2]]  #L->0
#for I Tetromino's case (clockwise direction)
case1_i = [[-2, 0], [1, 0], [-2, -1], [1, 2]] #0->R
case2_i = [[-1, 0], [2, 0], [-1, 2], [2, -1]] #R->2
case3_i = [[2, 0], [-1, 0], [2, 1], [-1, -2]] #2->L
case4_i = [[1, 0], [-2, 0], [1, -2], [-2, 1]] #L->0

#be careful with initializing 2-Dimensional array
#initialize board (new board)
#input:
#output: board[][]:int
def initBoard():
    board = [[0 for j in range(colSize)] for i in range(rowSize)]
    return board

#get random block
#input: shape[][][]:int (storing all shape of block), land[][]:int
#output: block:Block(object)
def randomBlock(shape):
    randomNumber = random.randint(0,6)
    randomShape = copy.deepcopy(shape[randomNumber])
    #block will display outside of land (more detail: block location will be higher than roof of land due to "-2" coordinate 'y')
    block = tetromino.Block(int(colSize/2) - len(randomShape[0]) + 1, 0, randomShape, randomNumber, 0)      #block will appear at the middle of the top of the land
    block.reInit()
    return block

#merge block with board (temporary board)
#input: board[][]:int, block:Block(object)
#output: board[][]:int
def mergeBlock(land,block):
    #merged board will be stored in temp variable ([][]int)
    temp = copy.deepcopy(land)
    for i in range(len(block.shape)):
        for j in range(len(block.shape[0])):
            if block.shape[i][j] != 0:
                #additional condition for display block even block is outside (above, higher from roof of board)
                if block.y + i <= rowSize - 1 and block.y + i >= 0 and block.x + j <= colSize - 1 and block.x + j >= 0:
                    temp[block.y+i][block.x+j] = block.shape[i][j]
    return temp

#collision between block and land (main board), between block and pieces of other previous block
#also the most important function
#input: land[][]:int, block:Block(object)
#output: True or False
def checkCollision(land,block):
    for i in range(len(block.shape)):
        for j in range(len(block.shape[0])):
            if block.shape[i][j] != 0:
                if block.x+j < 0 or block.x+j > colSize-1 or block.y+i > rowSize-1:
                    return True
                elif block.x+j >= 0 and block.x+j <= colSize - 1 and block.y+i >= 0 and block.y+i <= rowSize-1:
                    if land[block.y+i][block.x+j] != 0:
                        return True
    return False

#reach top??? we just need to check the first line at the top of the land
#if it has any piece, return True to alarm that the game will be over at the next move 
#input: land[][]:int
#output: True or False
def checkReachTop(land):
    for j in range(len(land[0])):
        if land[0][j] != 0:
            return True         #the game will be over
    return False                #the game is continuing
        
#check every single line of land array (main board)
#we will delete lines which is full by make it clear with 0 and swap very couple lines from i to 0 as (line[i] = line[i-1]), (line[i-1] = line[i-2]), ...
#swap from under line to top line 
#input: land[][]:int, score:int
#output: land[][]:int (after checking and re-merging), score:int 
def handleFullLine(land,score):
    flag = True
    line = []
    #collect all line was full
    for i in range(len(land)):
        flag = True
        for j in range(len(land[0])):
            if land[i][j] == 0:
                flag = False
                break
        if flag == True:
            line.append(i)
    
    for i in line:
        score += 100    #get score
        for j in range(len(land[0])):
            land[i][j] = 0
        #copy land's row which is clear with value 0
        temp = copy.deepcopy(land[i])
        #take that row out
        land.pop(i)
        #and insert it to the first position of array 'land'
        land.insert(0,temp)

    return land, score

#delay program for t second
#input: t:int
def sleep(t):
    time.sleep(t)

#clear console
def clearScreen():
    os.system("clear")

#in: arr:[][]int, nextblock:Block(object), score:int, color:[]string
#out: []string
def outputConvert(land,block,nextblock,score,color):
    arr = mergeBlock(land,block)
    #temporary variables for printing out next Block
    tempIndexRow = 0
    tempIndexCol = 0
    #output array type string
    strout = []
    tempstr = ""
    for i in range(len(arr)):
        #refresh
        tempIndexCol = 0
        tempstr = ""
        #
        if i == 0:
            strout.append("  " + "_ "*colSize + "  " + "  ")# + "  "*4 + " |")
        for j in range(len(arr[0])):
            if j == 0:
                tempstr += "||"
            if i == len(arr)-1 and arr[i][j] == 0:
                tempstr += "_ "
            else:
                tempstr += color[arr[i][j]]
            if j == len(arr[0])-1:
                tempstr += "||"
        # if i == 1:
        #     tempstr += "  Score: " + str(score)
        # elif i == 3:
        #     tempstr += "  Next"
        # elif i > 4 and tempIndexRow < len(nextblock.shape):
        #     tempstr += "  "
        #     while tempIndexCol < len(nextblock.shape[0]):
        #         tempstr += color[nextblock.shape[tempIndexRow][tempIndexCol]]
        #         tempIndexCol += 1
        #     if len(nextblock.shape[0]) < 4:
        #         tempstr += " "*(4-len(nextblock.shape[0]))
        #     tempstr += "  |"
        #     tempIndexRow += 1
         #   
        strout.append(tempstr)
        # maxLen = 160
        # # #find max length
        # # for i in range(len(strout)):
        # #     if maxLen < len(strout[i]):
        # #         maxLen = len(strout[i])
        
        # for i in range(len(strout)):
        #     if len(strout[i]) < maxLen:
        #         for j in range(len(strout[i]),maxLen):
        #             strout[i] += " "

    return strout         

#for multiple play
#input: arr1:[]string, arr2:[]string, ...
#output: []string
def mergeStringArray(arr1,arr2):
    #result array
    result = [""] * len(arr1)
    #all arrays have the same size
    for i in range(len(arr1)):
        result[i] += arr1[i] + "\t\t" + arr2[i]

    return result

#print out 2D array and score
#input: arr[][]:int, nextBlock:Block(object), score:int, color:[]string
def display(strout):
    clearScreen()
    for i in range(len(strout)):
        print("\r",strout[i])
    # #Game instuction
    # print("\rPress: W - Rotate, S - Fall down faster, A - Move left, D - Move Right")
    # print("\rPress: Spacebar - Hard Drop")
    # print("\rPress: X to exit the game ")

#thread get input from keyboard
def getKey():
    while True:
        key = readchar.readkey()
        inputQueue.put(key)
        if key == "x" or key == "X":
            sys.exit()

#do movement according to input
#input: land:[][]int; block:Block(object); inputQueue:Queue; score:int; levelUp:int; color:[]string, performArray2:[]string
#output: block:Block(object); inputQueue:Queue; key:string
#don't need to have parameter 'performArray2' if display() function runs every 0.055 second 
def doMovement(land,block,nextBlock,intputQueue,score,levelUp,color,performArray2):
    key = ""
    preSec = int(round(time.time()*1000))
    while not inputQueue.empty():
        key = inputQueue.get()
        if key == "w" or key == "W":
            block.rotate(land)
        elif key == "a" or key == "A":
            block.moveLeft(land)
        elif key == "d" or key == "D":
            block.moveRight(land)
        elif key == "s" or key == "S":
            block.moveDown(land)
        elif key == " ":
            block.hardDrop(land)
        elif key == "x" or key == "X":
            break
        else:
            continue
        performArray1 = outputConvert(land,block,nextBlock,score,color)
        strout = mergeStringArray(performArray1,performArray2)
        display(strout)
        sleep(0.01)
        #do movement in only 1 second (can be decreased due to increasing of scores)
        if int(round(time.time()*1000)) - preSec >= 1000 - levelUp or key == " ":
            break
    return block, inputQueue, key

'''
# idea:
# display function's input: tuple(land,block,nextblock,score) and call convert (to string) function for that tuple
# and merge 2 string result after
# 2 players platform
# input: data1:tuple, data2:tuple, color:string (for color changing)
# tuple = {land, block, nextblock, score}
def display(data1,data2,color):
    strtmp1 = outputConvert(data1[0],data1[1],data1[2],data1[3],color)
    strtmp2 = outputConvert(data2[0],data2[1],data2[2],data2[3],color)
    clearScreen()
    for i in range(len(strout)):
        print("\r" + strout[i])
'''
'''
# other idea:
# display function get 2 dimensional array of string
# we will store player 1's performance array in var1 and player 2's performance array in var2
# then merge it together and assign to var3
# put var3 (2 dimensional array type string) into display function to display the match
'''
